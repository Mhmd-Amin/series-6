package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator implements Runnable {
    private final int step;
    private final int scale;

    public Calculator(int index, int floatingPoint) {
        step = index;
        scale = floatingPoint;
    }
    @Override
    public void run() {
        cal();
    }

    private void cal() {
        BigDecimal sigma = new BigDecimal(0).setScale(scale);

        BigDecimal leftFraction = new BigDecimal(-1).setScale(scale);
        leftFraction = leftFraction.pow(step);
        leftFraction = leftFraction.divide(fastPow(step), RoundingMode.HALF_UP).setScale(scale);
        leftFraction = leftFraction.multiply(rightFractionsAdd());
        sigma = sigma.add(leftFraction);

        BigDecimal bigDecimal = new BigDecimal(0).setScale(scale);
        bigDecimal = bigDecimal.add(sigma);
        Bellards.end(bigDecimal);
    }

    private BigDecimal rightFractionsAdd() {
        BigDecimal total = new BigDecimal(0).setScale(scale);
        BigDecimal firstFraction = new BigDecimal(-32).setScale(scale);
        BigDecimal secondFraction = new BigDecimal(-1).setScale(scale);
        BigDecimal thirdFraction = new BigDecimal(256).setScale(scale);
        BigDecimal fourthFraction = new BigDecimal(-64).setScale(scale);
        BigDecimal fifthFraction = new BigDecimal(-4).setScale(scale);
        BigDecimal sixthFraction = new BigDecimal(-4).setScale(scale);
        BigDecimal seventhFraction = new BigDecimal(1).setScale(scale);

        firstFraction = firstFraction.divide(bigDecimalInit(4 * step + 1), RoundingMode.HALF_UP).setScale(scale);
        secondFraction = secondFraction.divide(bigDecimalInit(4 * step + 3), RoundingMode.HALF_UP).setScale(scale);
        thirdFraction = thirdFraction.divide(bigDecimalInit(10 * step + 1), RoundingMode.HALF_UP).setScale(scale);
        fourthFraction = fourthFraction.divide(bigDecimalInit(10 * step + 3), RoundingMode.HALF_UP).setScale(scale);
        fifthFraction = fifthFraction.divide(bigDecimalInit(10 * step + 5), RoundingMode.HALF_UP).setScale(scale);
        sixthFraction = sixthFraction.divide(bigDecimalInit(10 * step + 7), RoundingMode.HALF_UP).setScale(scale);
        seventhFraction = seventhFraction.divide(bigDecimalInit(10 * step + 9), RoundingMode.HALF_UP).setScale(scale);

        total = total.add(firstFraction);
        total = total.add(secondFraction);
        total = total.add(thirdFraction);
        total = total.add(fourthFraction);
        total = total.add(fifthFraction);
        total = total.add(sixthFraction);
        total = total.add(seventhFraction);

        return total;
    }

    private BigDecimal bigDecimalInit(int num) {
        BigDecimal bigDecimal = new BigDecimal(num).setScale(scale);
        return bigDecimal.setScale(scale);
    }

    private BigDecimal fastPow (int tavan) {
        BigDecimal total = new BigDecimal(1).setScale(scale);
        do {
            if (tavan >= 16) {
                //2^160
                BigDecimal t = new BigDecimal("1461501637330902918203684832716283019655932542976").setScale(scale);
                total = total.multiply(t.pow(tavan / 16));
                tavan = tavan % 16;
            }
            else if (tavan >= 8) {
                //2^80
                BigDecimal t = new BigDecimal("1208925819614629174706176").setScale(scale);
                total = total.multiply(t.pow(tavan / 8));
                tavan = tavan % 8;
            }
            else if (tavan >= 4) {
                //2^40
                BigDecimal t = new BigDecimal("1099511627776").setScale(scale);
                total = total.multiply(t.pow(tavan / 4));
                tavan = tavan % 4;
            }
            else if (tavan >= 2) {
                //2^20
                BigDecimal t = new BigDecimal("1048576").setScale(scale);
                total = total.multiply(t.pow(tavan / 2));
                tavan = tavan % 2;
            }
            else {
                //2^10
                BigDecimal t = new BigDecimal("1024").setScale(scale);
                total = total.multiply(t.pow(tavan));
                break;
            }
        } while (tavan != 0);
        return total.setScale(scale);
    }
}
