package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Bellards {
    private static int scale;
    private static BigDecimal sigma = new BigDecimal(0).setScale(scale);
    private static BigDecimal coefficient;

    public static void setScale(int floatingPoint) {
        scale = floatingPoint;
        coefficient = new BigDecimal(64).setScale(scale);

    }

    synchronized public static void end (BigDecimal num) {
        sigma = sigma.add(num);
    }

    public static BigDecimal getPi() {
        sigma = sigma.divide(coefficient, RoundingMode.HALF_UP);
        return sigma;
    }
}
