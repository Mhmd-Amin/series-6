package sbu.cs.multithread.pi;

public class PICalculator {
    public String calculate(int floatingPoint) {
        String Pi = TreadPool.piCal(floatingPoint + 1);
        return Pi.substring(0, Pi.length() - 1);
    }
}
