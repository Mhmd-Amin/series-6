package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TreadPool {
    private static final ExecutorService trds = Executors.newFixedThreadPool(7);
    private static int scale;
    private static int steps;

    public static String piCal(int floatingPoint) {
        setAll(floatingPoint);
        Bellards.setScale(scale);
        BigDecimal Pi;
        for (int i = 0; i <= steps; i++) {
            trds.submit(new Calculator(i, scale));
        }
        trds.shutdown();
        try {
            trds.awaitTermination(800000, TimeUnit.MILLISECONDS);
        }
        catch (InterruptedException ie) {
        }
        Pi = Bellards.getPi().setScale(floatingPoint, RoundingMode.HALF_UP);
        return Pi.toString();
    }

    private static void setAll(int floatingPoint) {
        steps = (int) Math.ceil((floatingPoint / 3));
        scale = (steps * 11) + 10;
    }
}
