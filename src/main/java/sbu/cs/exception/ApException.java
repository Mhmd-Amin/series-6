package sbu.cs.exception;

public class ApException extends Exception {
    public ApException() {}

    public ApException(String message) {
        super(message);
    }
}
