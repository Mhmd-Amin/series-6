package sbu.cs.exception;

public class NotFoundCommandException extends ApException {
    public NotFoundCommandException() {}

    public NotFoundCommandException(String message) {
        super(message);
    }
}
