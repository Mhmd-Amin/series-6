package sbu.cs.exception;

public class NotImplementCommandException extends ApException {
    public NotImplementCommandException() {}

    public NotImplementCommandException(String message) {
        super(message);
    }
}
