package sbu.cs.exception;

public class BadInputException extends ApException {
    public BadInputException() { }

    public BadInputException(String message) {
        super(message);
    }
}
